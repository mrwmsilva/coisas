#!/bin/bash
# Start MongoDB Cluster

# Declaracao das variaveis
# Hostnames
PRIMARY="mongodb1"
SECONDARY="
mongodb2
mongodb3
mongodb4
mongodb5
"
# Nome do replica set
RPLSET="rsAPI"

# Verifica existencia do diretorio de persistencia
for S in $PRIMARY $SECONDARY ; do if [ ! -d $S ] ; then mkdir $S ; chmod 777 $S ; fi ; done

# Gerenciamento dos containers
for S in $PRIMARY $SECONDARY ; do

  # Verifica se o container esta criado e rodando para finalizar
  if [ $(docker ps -a | grep -q $S ; echo $?) = 0 ] ; then
    if [ $(docker ps | grep -q $S ; echo $?) = 0 ] ; then
      echo "Finalizando container $S"
      docker kill $S
    fi
      echo "Removendo container $S"
      docker rm $S
  fi

  # Iniciando containers
  echo "Iniciando container $S"
  docker run -P -v "$(pwd)"/"$S":/data/db --name "$S" --hostname="$S" -d mongo --replSet "$RPLSET" --noprealloc --smallfiles
done

# Remove getip
if [ -e getip.txt ] ; then rm -rf getip.txt ; fi

# Extrai o endereco IP dos containers
for S in $PRIMARY $SECONDARY ; do
  echo $(docker inspect --format '{{ .NetworkSettings.IPAddress }}' "$S") "$S" >> getip.txt
done

# Cria script para acerto do /etc/hosts nos containers
if [ -e updateHost.sh ] ; then rm -rf updateHost.sh ; fi
echo "#!/bin/bash
cat /etc/hosts > /etc/hosts1
cat /etc/getip.txt >> /etc/hosts1
cat /etc/hosts1 > /etc/hosts" > updateHost.sh

# Copia para os containers o arquivo getip.txt, o script, ajusta a permissao e executa
for S in $PRIMARY $SECONDARY ; do
  docker cp updateHost.sh "$S":/etc
  docker cp getip.txt "$S":/etc
  docker exec -it "$S" chmod +x /etc/updateHost.sh
  docker exec -it "$S" /etc/updateHost.sh
done

# Inicia o Replica Set
docker exec -it "$PRIMARY" mongo --eval "rs.status()"
docker exec -it "$PRIMARY" mongo --eval "db"
docker exec -it "$PRIMARY" mongo --eval "rs.initiate()"
sleep 120
for S in $SECONDARY ; do
  docker exec -it "$PRIMARY" mongo --eval "rs.add(\"$S:27017\")"
  docker exec -it "$S" mongo --eval "rs.slaveOk()"
done

# Exibe status do cluster
docker exec -it "$S" mongo --eval "rs.status()"
